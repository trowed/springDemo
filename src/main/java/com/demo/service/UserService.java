package com.demo.service;

import com.spring.Autowired;
import com.spring.Component;
import com.spring.InitalizingBean;
import com.spring.Scope;

/**
 * @author ssh
 * @version 1.0
 * @date 2021/12/20 20:25
 */
@Component("userService")
@Scope("singleton")
public class UserService implements UserInterFace {

    @Autowired
    private OrderService orderService;

     @Override
    public void test() {
        System.out.println(orderService);
    }

    public void afterPropertiesSet() {
        System.out.println("初始化");
    }
}
