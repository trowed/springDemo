package com.demo.service;

import com.spring.BeanPostProcessor;
import com.spring.Component;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * @author ssh
 * @version 1.0
 * @date 2021/12/20 22:22
 */
@Component
public class DemoBeanPostProcess implements BeanPostProcessor {

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) {
        if ("userService".equals(beanName)) {
            Object proxyInstance = Proxy.newProxyInstance(DemoBeanPostProcess.class.getClassLoader(), bean.getClass().getInterfaces(), new InvocationHandler() {
                @Override
                public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                    //切面
                    System.out.println("切面逻辑");
                    return method.invoke(bean,args);
                }
            });
              return proxyInstance;
        }
        return null;
    }

}
