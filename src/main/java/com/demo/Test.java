package com.demo;

import com.spring.DemoApplicationContext;
import com.demo.service.UserInterFace;

/**
 * @author ssh
 * @version 1.0
 * @date 2021/12/20 20:24
 */
public class Test {

    public static void main(String[] args) {

        //扫描-->创建非懒加载的单例Bean
        DemoApplicationContext applicationContext = new DemoApplicationContext(AppConfig.class);
        UserInterFace userService = (UserInterFace) applicationContext.getBean("userService");
        userService.test();
    }
}
