package com.demo;

import com.spring.ComponentScan;

/**
 * @author ssh
 * @version 1.0
 * @date 2021/12/20 20:27
 */
@ComponentScan("com.demo.service")
public class AppConfig {
}
