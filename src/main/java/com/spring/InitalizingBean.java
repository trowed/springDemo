package com.spring;

/**
 * @author ssh
 * @version 1.0
 * @date 2021/12/20 22:17
 */
public interface InitalizingBean {

    void afterPropertiesSet() ;
}
