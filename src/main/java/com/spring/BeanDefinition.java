package com.spring;

/**
 * @author ssh
 * @version 1.0
 * @date 2021/12/20 21:06
 */
public class BeanDefinition {

    /**
     * 类型
     */
    private Class type;
    /**
     * 作用域
     */
    private String scope;

    private boolean isLazy;

    public Class getType() {
        return type;
    }

    public void setType(Class type) {
        this.type = type;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public boolean isLazy() {
        return isLazy;
    }

    public void setLazy(boolean lazy) {
        isLazy = lazy;
    }
}
