package com.spring;

/**
 * @author ssh
 * @version 1.0
 * @date 2021/12/20 22:17
 */
public interface BeanPostProcessor {

    default Object postProcessBeforeInitialization(Object bean,String beanName){
        return bean;
    }

     default Object postProcessAfterInitialization(Object bean,String beanName){
        return bean;
    }
}
